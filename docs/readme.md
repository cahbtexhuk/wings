# Docs
## PDFs
* STM32F303 datasheet, reference manual and programming manual - downloaded from [ST website] (http://www.st.com/web/catalog/mmc/FM141/SC1169/SS1576/LN1531/PF252054)  

# Libraries
* MPU6050 library - downloaded from [harinadha.wordpress.com](https://harinadha.wordpress.com/2012/05/23/mpu6050lib/)
