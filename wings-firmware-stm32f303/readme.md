# Firmware
This is a firmware part of the project
## Overview
Firmware is built for STM32F303VC processor and utilizes various other peripheral components.  
Development is being done on [STM32F303 series Discovery board](http://www.st.com/web/en/catalog/tools/PF254044)  
Software is based on [FreeRTOS 8.2.1](http://www.freertos.org/) operating system and CooCox St peripheral libraries.
## Todo list
* ~~Launch FreeRTOS on Discovery board~~
* Implement and test drivers:
  * GPIO
  * I2C
  * USART
  * SPI
  * ADC  
* Port and test MPU6050 library to CoIDE/STM32F303VC
* Implement MPU6050 data processing
* Implement multiple MPU6050 reading and processing (may require additional "nodes")
* Implement sensor calibration and data storage on EEPROM
* Implement servo multipled driver using 3-to-8 decoders
* Implement remote monitoring/control using USART or SPI radio modules (XBee or NRF24)
* Implement ADC battery monitoring
* Implement wing control logic (TBD)
* ???