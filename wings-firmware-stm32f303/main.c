/*
 * Includes
 */

#include "stm32f30x.h"
#include "stm32f30x_rcc.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_misc.h"
#include "FreeRTOS.h"
#include "task.h"
#include "mpu_wrappers.h"
#include "MPU6050.h"


// definitions
static void prvSetupHardware( void );
void prvFlashTask(void *pvParameters);
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName );

// main stuff
int main(void)
{
	volatile int x;
	x=0;
	MPU6050_I2C_Init();
	MPU6050_Initialize();
	if( MPU6050_TestConnection()== true)
	{
	   // connection success
		x++;
	}else{
	   // connection failed
		x--;
	}
	xTaskCreate( prvFlashTask, "Flash", 40, NULL, 1, NULL );
	vTaskStartScheduler();

    while(1)
    {

    }
}

static void prvSetupHardware( void )
{
	SystemInit();

	/* Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE and AFIO clocks */
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA  | RCC_AHBPeriph_GPIOB  | RCC_AHBPeriph_GPIOC
							| RCC_AHBPeriph_GPIOD  | RCC_AHBPeriph_GPIOE , ENABLE );

	/* SPI2 Periph clock enable */
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2, ENABLE );


	/* Set the Vector Table base address at 0x08000000 */
	NVIC_SetVectorTable( NVIC_VectTab_FLASH, 0x0 );

	NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );

	/* Configure HCLK clock as SysTick clock source. */
	SysTick_CLKSourceConfig( SysTick_CLKSource_HCLK );

}

void prvFlashTask(void *pvParameters){
	//init stuff
	volatile int x=0;
	for(;;)	//run loop
	{
		x++;
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	/* This function will get called if a task overflows its stack.   If the
	parameters are corrupt then inspect pxCurrentTCB to find which was the
	offending task. */

	( void ) pxTask;
	( void ) pcTaskName;

	for( ;; );
}
