#ifndef BUS_I2C_H
#define BUS_I2C_H

/***********************************
 * global includes etc
 ***********************************/
#include <stdint.h>
#include <stdbool.h>
#include "stm32f30x.h"
#include "stm32f30x_rcc.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_i2c.h"
/***********************************
 * definitions
 ***********************************/
void i2cBusInit(I2C_TypeDef* I2Cx);
void i2cWriteByte(I2C_TypeDef* I2Cx, uint8_t slaveAddr, uint8_t *pData, uint8_t writeAddr);
void i2cReadBuffer(I2C_TypeDef* I2Cx, uint8_t slaveAddr, uint8_t *pData, uint8_t readAddr, uint16_t amount);
// TODO: move to proper place
#define MPU6050_ADDRESS_AD0_LOW     0x68 // address pin low (GND), default for InvenSense evaluation board
#define MPU6050_ADDRESS_AD0_HIGH    0x69 // address pin high (VCC)
#define MPU6050_DEFAULT_ADDRESS     (MPU6050_ADDRESS_AD0_LOW<<1)

#define sEEAddress MPU6050_DEFAULT_ADDRESS
// TODO: calculate proper values
#define I2C_LONG_TIMEOUT         ((uint32_t)(10 * I2C_FLAG_TIMEOUT))
#endif
