#include "bus_i2c.h"

void initI2CGPIO(I2C_TypeDef* I2Cx) {
	// TODO
}

void I2C_TIMEOUT_UserCallback(){
	return;
}

void i2cBusInit(I2C_TypeDef* I2Cx)
{
	static unsigned int I2CTimeout;

	I2C_InitTypeDef I2C_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	uint32_t RCC_APB1Periph_I2Cx;
	// define which i2c we are using
	I2Cx == I2C1 ? (RCC_APB1Periph_I2Cx = RCC_APB1Periph_I2C1) : (RCC_APB1Periph_I2Cx = RCC_APB1Periph_I2C2);

	I2C_Cmd(I2Cx, DISABLE);
	I2C_DeInit(I2Cx);

	//TODO: initGPIO(I2Cx); - move code below to separate function

	// Enable peripheral clock using RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2Cx, ENABLE)
	// function for I2C1 or I2C2.
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2Cx, ENABLE);

	// Enable SDA, SCL  and SMBA (when used) GPIO clocks using
	// RCC_AHBPeriphClockCmd() function.
	RCC_APB2PeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

	// (#) Peripherals alternate function:
	// (++) Connect the pin to the desired peripherals' Alternate
	// Function (AF) using GPIO_PinAFConfig() function.
	GPIO_PinAFConfig(GPIOB, GPIO_Pin_6 | GPIO_Pin_7, GPIO_AF_4);
	// (++) Configure the desired pin in alternate function by:
	// GPIO_InitStruct->GPIO_Mode = GPIO_Mode_AF
	// (++) Select the type, OpenDrain and speed via
	// GPIO_PuPd, GPIO_OType and GPIO_Speed members

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	// (++) Call GPIO_Init() function.
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	// (#) Program the Mode, Timing , Own address, Ack and Acknowledged Address
	// using the I2C_Init() function.

	//I2C_InitStructure
	 /*!< I2C configuration */
	/* sEE_I2C configuration */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
	I2C_InitStructure.I2C_DigitalFilter = 0x00;
	I2C_InitStructure.I2C_OwnAddress1 = 0x00;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_Timing = 0xC062121F; // magic value

	/* Apply sEE_I2C configuration after enabling it */
	I2C_Init(I2Cx, &I2C_InitStructure);

	/* sEE_I2C Peripheral Enable */
	I2C_Cmd(I2Cx, ENABLE);

//	/*!< Select the EEPROM address */
//	sEEAddress = sEE_HW_ADDRESS;
}

void i2cWriteByte(I2C_TypeDef* I2Cx, uint8_t slaveAddr, uint8_t *pData, uint8_t writeAddr)
{
	// TODO
}

void i2cReadBuffer(I2C_TypeDef* I2Cx, uint8_t slaveAddr, uint8_t *pData, uint8_t readAddr, uint16_t amount)
{
	static unsigned int I2CTimeout;
	uint32_t NumbOfSingle = 0, Count = 0, DataNum = 0, StartCom = 0;

	/* Get number of reload cycles */
	Count = (amount) / 255;
	NumbOfSingle = (amount) % 255;

	/* Configure slave address, nbytes, reload and generate start */
	I2C_TransferHandling(I2Cx, slaveAddr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);

	/* Wait until TXIS flag is set */
	I2CTimeout = I2C_LONG_TIMEOUT;
	while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TXIS) == RESET) {
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
	}

	/* Send memory address */
	I2C_SendData(I2Cx, (uint8_t)readAddr);

	/* Wait until TC flag is set */
	I2CTimeout = I2C_LONG_TIMEOUT;
	while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TC) == RESET) {
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
	}

	/* If number of Reload cycles is not equal to 0 */
	if (Count != 0) {
		/* Starting communication */
		StartCom = 1;

		/* Wait until all reload cycles are performed */
		while( Count != 0) {
			/* If a read transfer is performed */
			if (StartCom == 0) {
				/* Wait until TCR flag is set */
				I2CTimeout = I2C_LONG_TIMEOUT;
				while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TCR) == RESET) {
					if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
				}
			}
			/* if remains one read cycle */
			if ((Count == 1) && (NumbOfSingle == 0)) {
				/* if starting communication */
				if (StartCom != 0) {
					/* Configure slave address, end mode and start condition */
					I2C_TransferHandling(I2Cx, sEEAddress, 255, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
				} else {
					/* Configure slave address, end mode */
					I2C_TransferHandling(I2Cx, sEEAddress, 255, I2C_AutoEnd_Mode, I2C_No_StartStop);
				}
			} else {
				/* if starting communication */
				if (StartCom != 0) {
					/* Configure slave address, end mode and start condition */
					I2C_TransferHandling(I2Cx, sEEAddress, 255, I2C_Reload_Mode, I2C_Generate_Start_Read);
				} else {
					/* Configure slave address, end mode */
					I2C_TransferHandling(I2Cx, sEEAddress, 255, I2C_Reload_Mode, I2C_No_StartStop);
				}
			}

			/* Update local variable */
			StartCom = 0;
			DataNum = 0;

			/* Wait until all data are received */
			while (DataNum != 255) {
				/* Wait until RXNE flag is set */
				I2CTimeout = I2C_LONG_TIMEOUT;
				while(I2C_GetFlagStatus(I2Cx, I2C_ISR_RXNE) == RESET) {
					if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
				}
				/* Read data from RXDR */
				pData[DataNum]= I2C_ReceiveData(I2Cx);

				/* Update number of received data */
				DataNum++;
				(amount)--;
			}
			/* Update Pointer of received buffer */
			pData += DataNum;

			/* update number of reload cycle */
			Count--;
		}

		/* If number of single data is not equal to 0 */
		if (NumbOfSingle != 0) {
			/* Wait until TCR flag is set */
			I2CTimeout = I2C_LONG_TIMEOUT;
			while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TCR) == RESET) {
				if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
			}
			/* Update CR2 : set Nbytes and end mode */
			I2C_TransferHandling(I2Cx, sEEAddress, (uint8_t)(NumbOfSingle), I2C_AutoEnd_Mode, I2C_No_StartStop);

			/* Reset local variable */
			DataNum = 0;

			/* Wait until all data are received */
			while (DataNum != NumbOfSingle) {
				/* Wait until RXNE flag is set */
				I2CTimeout = I2C_LONG_TIMEOUT;
				while(I2C_GetFlagStatus(I2Cx, I2C_ISR_RXNE) == RESET) {
					if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
				}

				/* Read data from RXDR */
				pData[DataNum]= I2C_ReceiveData(I2Cx);

				/* Update number of received data */
				DataNum++;
				(amount)--;
			}
		}
	} else 	{
		/* Update CR2 : set Slave Address , set read request, generate Start and set end mode */
		I2C_TransferHandling(I2Cx, sEEAddress, (uint32_t)(NumbOfSingle), I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

		/* Reset local variable */
		DataNum = 0;

		/* Wait until all data are received */
		while (DataNum != NumbOfSingle) {
			/* Wait until RXNE flag is set */
			I2CTimeout = I2C_LONG_TIMEOUT;
			while(I2C_GetFlagStatus(I2Cx, I2C_ISR_RXNE) == RESET) {
				if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
			}

			/* Read data from RXDR */
			pData[DataNum]= I2C_ReceiveData(I2Cx);

			/* Update number of received data */
			DataNum++;
			(amount)--;
		}
	}

	/* Wait until STOPF flag is set */
	I2CTimeout = I2C_LONG_TIMEOUT;
	while(I2C_GetFlagStatus(I2Cx, I2C_ISR_STOPF) == RESET) {
		if((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback();
	}

	/* Clear STOPF flag */
	I2C_ClearFlag(I2Cx, I2C_ICR_STOPCF);

	/* If all operations OK, return sEE_OK (0) */
	return 0;
}
