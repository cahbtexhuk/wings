# Artificial Wings
## Project overview
Animatronic wings, built using STM32F303VC MCU to control various servos, using data acquired from MPU6050 IMUs, mounted on users arms.  
Firmware is being developed using CooCox CoIDE and ST STM32F3DISCOVERY dev board
## Project structure
* *docs* - datasheets, notes, algorhytm descriptions, links etc to useful resource
* *wings-frimware-xxx* - MCU firmware
* *utilites* - utilities to configure, monitor and remotely control wings
* *hardware* - electronics and mechanical hardware for project  

Each subfolder has it's own README.md with further details

## Todo lists
Each subfolder has separate todo list.

## Licenses and disclaimer
This project is being built for personal use. Feel free to use and/or modify any part of it at your own risk, because I do not hold any responsibilities if you get shocked or accidentally destroy something or injure or kill someone.  
If you use any part of this project and you find it useful - feel free to donate.  
This project uses various sources and libraries available in open access. Each source used will be referred and it's licese (if applicable) is used.